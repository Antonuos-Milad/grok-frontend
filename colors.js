import React from 'react';
import { StyleSheet } from 'react-native';


export default styles = StyleSheet.create({
   headerColor : {
        color : '#fd6f3f',
    
    } , contentColor : {
        color: '#ffffff'      
    },
    
    inputColor:{
        color : '#ffffff',
    },

    buttonsColor : {
        color :  '#1D59CF',
    },
   
        
});
