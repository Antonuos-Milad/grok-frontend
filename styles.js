import { StyleSheet } from "react-native";
import React from 'react';
export default styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignSelf: "stretch"
    },
    page: {
        flex: 1
    }
});