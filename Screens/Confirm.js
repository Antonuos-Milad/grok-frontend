import React, { Component } from 'react';
import {
    TextInput,
    StyleSheet,
    SafeAreaView,
    Alert,
    TouchableOpacity,
    Text,
    KeyboardAvoidingView,
    ToastAndroid,
    View,
    Image,
    Linking,
} from 'react-native';
import { Dimensions} from 'react-native'
import Colors from '../colors';
import { underline, bold } from 'ansi-colors';
import * as Font from 'expo-font';
import { AppLoading } from 'expo';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import { ScrollView } from 'react-native-reanimated';
// import Icon from 'react-native-vector-icons/FontAwesome';
// import { Ionicons } from '@expo/vector-icons';
//import SafeAreaView from 'react-native-safe-area-view';

export default class Confirm extends Component {
    static navigationOptions =
    {
      headerVisible: false,
      header: null,
    };

    state =
        {
            //loginPassword: '', 
            code:'',  
            userObj: {},
            codes:[],  //the list of codes from DB
            codeExist:false,   //does the code exist in db ?

            assetsLoaded: false,
        }

        async componentDidMount() {
            await Font.loadAsync({
                'Poppins': require('../assets/Fontt/Poppins-Bold.ttf'),
                'Poppins': require('../assets/Fontt/Poppins-Light.ttf'),
                'Poppins': require('../assets/Fontt/Poppins-SemiBold.ttf')
            });
        
            this.setState({ assetsLoaded: true });
        }

         //handle login 
    LoginHandler = () => {
          var url = 'http://192.168.137.91/api/Confirm';
          fetch(url) // get the list of codes
              .then(response => response.json())
              .then(codes => {
                  this.setState({ codes });
                  
              }
              )
              .then(() => {
                  //check if the user exists in the list of users
                  this.state.codes.forEach((item) => {
                      
                      if (this.state.code.localeCompare(item.Codenumber) == 0) {
                          this.setState({ codeExist: true });
                          this.setState({ userObj: item });
                          
                          return;
                      }
                  });
              })
              .then(() => {
                  // handle if the user exists or not
                  if (this.state.codeExist == true) {
                      ToastAndroid.showWithGravity(
                          'Success',
                          ToastAndroid.SHORT,
                          ToastAndroid.BOTTOM,                              
                      );
                                  //navigate to next screen with params need here the expected params need not code it user
                      this.props.navigation.navigate('completeSignUp', { code: this.state.userObj });
                  //to navigate to next screen 
                      this.props.navigation.navigate('completeSignUp');
                  }
                  else {
                      Alert.alert(
                          ' ', // alert title
                          'there is an error in entered code , please try again', // alert message
                          [
                              {
                                  text: 'حسنا'//, onPress: () => console.log('Ask me later pressed') 
                              },
                          ],
                          { cancelable: true },
                      );
                  }
              })
              .catch(error => console.error('Error occured :: ', error));
      }
  
     
 render() {
    return (

        <SafeAreaView style={styles.container}> 

{/* //need to wrap it in a clickable element, like a TouchableOpacity, TouchableHighlight */}
<TouchableHighlight 
onPress={ ()=> { navigation.navigate('phoneNumber') } }>
  <Image  
    source={require('../assets/images/leftarrows/arrowLeft2x.png')}
    style={{height:20, width:15,marginTop:22,marginLeft:23}} />
</TouchableHighlight>

              <Text  fontfamily="Poppins-Bold" style={{
                     backgroundColor: Colors.headerColor.color,
                    // fontfamily:'Poppins',
                     alignSelf: 'flex-start',
                     textAlign: 'left',
                    //  justifyContent:'center',
                     fontSize:32,
                     marginTop:0,
                     marginLeft:22,
                     marginTop:60,
                     color:'#ffffff'}}>Singup</Text> 
          
                      
                      {/* //seprator line */}
                      <View style={{flex: 1, flexDirection: 'row',justifyContent: 'space-between',marginLeft:22,marginRight:23,marginTop:15}}>
                      <View style={{width: 95, height: 6, backgroundColor: '#1D59CF',borderRadius: 10,}} />
                      <View style={{width: 95, height: 6, backgroundColor: '#1D59CF',borderRadius: 10,}} />
                      <View style={{width: 95, height: 6, backgroundColor: '#1D59CF',borderRadius: 10,}} />
                    </View> 


                <View>
                      <Image source={require('../assets/images/mobileIcon/V2.png')}
                         //Image Style                       
                         style={styles.ImageIconStyle} /> 
                 </View>  

        <KeyboardAvoidingView behavior='position' enabled style={{  marginTop:100, }}>   
                    <TextInput
                        placeholder='Verify Code'
                        placeholderTextColor='#BFBFBF'
                        style={styles.inputs}
                        value={this.state.code}
                        onChangeText={(text) => {
                            this.setState({ code: text })
                        }
                        }
                     />
              </KeyboardAvoidingView>
             

              {/* //Code Verify Button */}
                     <TouchableOpacity  
                         onPress={this.LoginHandler}
                         style={styles.confirmStyle}
                         placeholder='Verify Code'
                         activeOpacity={0.5}>                         
                         <Text style={styles.confirmTextStyle} > Verify</Text>
                         <View style={styles.confirmSeparatorLine} />
                         <Image source={require('../assets/images/forwardarrow/forward2.png')}
                         //Image Style                       
                         style={styles.confirmImageIconStyle} />                                     
                     </TouchableOpacity>     

        </SafeAreaView>
    );
}
}
const { width } = Dimensions.get('window')
const styles = StyleSheet.create(
        {
            container: {
                flex:1,
                justifyContent: 'center',
                fontSize:15,
                backgroundColor: '#fd6f3f',
                color: Colors.inputColor.color,
            },

            inputs: {
                width: '90%',
                padding: 3,
                marginTop: 50,
                marginLeft: 15,
                marginBottom: 15,
                borderBottomWidth: 1,
               // borderBottomWidth: StyleSheet.hairlineWidth,
                borderRadius: 8,
                borderBottomColor: '#ffffff',
                color: '#ffffff',
                textAlign: 'left',
                fontSize:15,
               // fontWeight:'bold',
                justifyContent:'flex-start',    
            },

              confirmStyle: {
                flexDirection: 'row',
                alignItems: 'center',
                alignContent: 'center',
                justifyContent: 'space-around',
                textAlign: 'left',
                borderColor: '#1D59CF',
                backgroundColor: '#1D59CF',
                borderWidth: 1,
                borderRadius: 10, 
                fontSize: 18,
                fontFamily:'Poppins',
                width: 320,
                height:50,
                padding: 10,
                marginLeft: 20,
                marginTop: 70,
                marginBottom:20,
                backgroundColor: Colors.buttonsColor.color,
              },

              confirmTextStyle: {
                color: '#ffffff',
                marginBottom:2,
                marginRight: 130,
                textAlign: 'left',
                fontSize: 18,
                fontFamily:'Poppins',
              },

              confirmSeparatorLine: {
                backgroundColor: 'white',
                width: 1,
                height: 40,
                marginLeft:60,
                marginRight:2,
              },
              confirmImageIconStyle: {
              //  padding: 5,
                margin: 5,
                height: 25,
                width: 25,
                resizeMode: 'stretch',
              },

             ImageIconStyle:{
               alignContent:'center',
               height:180,
               width:116,
               justifyContent:'center',
               resizeMode:'stretch',
               marginLeft:130,
               marginBottom:10,
            
              },
  
        }
    );

