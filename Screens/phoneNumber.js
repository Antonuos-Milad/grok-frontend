import React, { Component } from 'react';
import {
    TextInput,
    StyleSheet,
    SafeAreaView,
    Alert,
    TouchableOpacity,
    Text,
    KeyboardAvoidingView,
    ToastAndroid,
    View,
    Image,
    StatusBar,
    ScrollView,
    useState,
    Button,
    Slider,
    TouchableHighlight,
} from 'react-native';
//import { Constants } from 'expo';
// import Icon from 'react-native-vector-icons/FontAwesome';
//import SafeAreaView from 'react-native-safe-area-view';
//import { HeaderBackButton } from 'react-navigation-stack';
import { Ionicons} from '@expo/vector-icons';
import { Dimensions} from 'react-native'
import Colors from '../colors';
import { underline, bold } from 'ansi-colors';
import * as Font from 'expo-font';
// import { AppLoading } from 'expo';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
//import DatePicker from 'react-native-datepicker';
// import DateTimePickerModal from "react-native-modal-datetime-picker";
// import DateTimePicker from '@react-native-community/datetimepicker';


export default class phoneNumber extends Component {

  static navigationOptions =
  {
    headerVisible: false,
    header: null,
  };

  async componentDidMount() {
        await Font.loadAsync({
            'Poppins': require('../assets/Fontt/Poppins-Bold.ttf'),
            'Poppins': require('../assets/Fontt/Poppins-Light.ttf'),
            'Poppins': require('../assets/Fontt/Poppins-SemiBold.ttf')
        });
    
        this.setState({ assetsLoaded: true });
    }
  
    state = 
    {
         phone_number: '',
         phoneValid: false,
         userValidNo: 0,
    }
  

//for set state for textinput
    onChangeText = (key, val) => {
        this.setState({ [key]: val })
   }


   signUp = () => 
   {    
    
        if(this.state.userValidNo == 1)
        {
             let user = JSON.stringify(
                  {      
                        phone: this.state.phone_number,
                  });
            

             let url = 'http://192.168.137.91/api/signup';
             fetch(url,
                  {
                       method: 'POST', // or 'PUT'
                       body: user,
                       headers: ({
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                       })
                  })
                  .then(res => res.json())
                  .then(response => {
                       ToastAndroid.showWithGravity(
                            'تم اضافه رقم هاتفك بنجاح ',
                            ToastAndroid.SHORT,
                            ToastAndroid.BOTTOM,
                       );
                       this.props.navigation.navigate('Confirm');
                  })
                  .catch(error => console.error('Failure signing up ::', error));
        }
        
        else
        {
             Alert.alert(
                  ' ', // alert title
                  'البيانات التي أدخلتها غير صحيحة، يرجى التأكد منها واعادة المحاولة', // alert message
                  [
                       {
                            text: 'حسنا'
                       }
                  ],
                  { cancelable: true },
             );
        }
   
   }

// validate user inputs
validate(text, type)
{
     if (type == 'phone_number') 
     {
          phoneRegex = /(01)[0-9]{9}/g;
          if (phoneRegex.test(text)) {
               this.setState({ phoneValid: true, userValidNo: this.state.userValidNo + 1 });    
          }
          else 
          {
               Alert.alert(
                    ' ', // alert title
                    'يجب أن لا يقل ولا يزيد رقم الهاتف عن 11 رقم، من فضلك حاول مرة أخرى', // alert message
                    [
                         {
                              text: 'حسنا'
                         }
                    ],
                    { cancelable: true },
               );
          }

  }

}  

//starting the view 
    render() {
        const { show, date, mode } = this.state;
        //start of data functons
        return (
<SafeAreaView style={styles.container}>

<Text  fontfamily="Poppins-Bold" style={{backgroundColor: Colors.headerColor.color,
       //fontfamily:'Poppins',
       alignSelf: 'flex-start',
       textAlign: 'left',
       //justifyContent:'center',
       fontSize:34,
       marginTop:0,
       marginLeft:20,
       marginTop:60,
       color:'#ffffff'}}>Singup</Text>            
     
            
     {/* //seprator line */}
    <View style={{flex: 1, flexDirection: 'row',justifyContent: 'space-between',marginLeft:20,marginRight:25,marginTop:15}}>
    <View style={{width: 95, height: 6, backgroundColor: '#1D59CF',borderRadius: 10,}} />
    <View style={{width: 95, height: 6, backgroundColor: '#fff',borderRadius: 10,}} />
    <View style={{width: 95, height: 6, backgroundColor: '#fff',borderRadius: 10,}} />
    </View>   


    <View>
         <Image source={require('../assets/images/mobileIcon/icon2.png')}
               //Image Style                       
            style={styles.ImageIconStyle} /> 
      </View>  

<KeyboardAvoidingView behavior='position' enabled style={{  marginTop:130, }}>                         
    <TextInput
         style={styles.inputs}
         placeholder='Phone Number'
         autoCapitalize="none"
         placeholderTextColor='#BFBFBF'
         keyboardType='numeric'
         value = {this.state.phone_number}
         onChangeText={val => this.onChangeText('phone_number', val)}
         onEndEditing={(e) => this.validate(e.nativeEvent.text, 'phone_number')} />                            
</KeyboardAvoidingView>  

{/* //Phonenumber Confirm Button */}
    <TouchableOpacity  
         onPress={this.handleSignUp}
         style={styles.confirmStyle}
         activeOpacity={0.5}>                         
         <Text style={styles.confirmTextStyle} > Verify </Text>
         <View style={styles.confirmSeparatorLine} />
         <Image source={require('../assets/images/forwardarrow/forward2.png')}
               //Image Style                       
                style={styles.confirmImageIconStyle} />                                     
    </TouchableOpacity>                          

    </SafeAreaView>
       );
    }
}

const { width } = Dimensions.get('window')
const styles = StyleSheet.create(
        {
            container: {
                flex:1,
                justifyContent: 'center',
                fontSize:15,
                backgroundColor: '#fd6f3f',
                color: Colors.inputColor.color,
            },

            inputs: {
                width: '90%',
                padding: 3,
                marginTop: 10,
                marginLeft: 15,
                marginBottom: 15,
                borderBottomWidth: 1,
               // borderBottomWidth: StyleSheet.hairlineWidth,
                borderRadius: 8,
                borderBottomColor: '#ffffff',
                color: '#ffffff',
                textAlign: 'left',
                fontSize:15,
               // fontWeight:'bold',
                justifyContent:'flex-start', 
            },

              confirmStyle: {
                flexDirection: 'row',
                alignItems: 'center',
                alignContent: 'center',
                justifyContent: 'space-around',
                textAlign: 'left',
                borderColor: '#1D59CF',
                backgroundColor: '#1D59CF',
                borderWidth: 1,
                borderRadius: 10, 
                fontSize: 18,
                fontFamily:'Poppins',
                width: 320,
                height:50,
                padding: 10,
                marginLeft: 20,
                marginTop: 15,
                marginBottom:20,
                backgroundColor: Colors.buttonsColor.color,
              },

              confirmTextStyle: {
                color: '#ffffff',
                marginBottom:2,
                marginRight: 130,
                textAlign: 'left',
                fontSize: 18,
                fontFamily:'Poppins',
              },

              confirmSeparatorLine: {
                backgroundColor: 'white',
                width: 1,
                height: 40,
                marginLeft:1,
                marginRight:2,
              },
              confirmImageIconStyle: {
              //  padding: 5,
                margin: 5,
                height: 25,
                width: 25,
                resizeMode: 'stretch',
              },

              ImageIconStyle:{
             alignContent:'center',
               height:200,
               width:130,
               justifyContent:'center',
               resizeMode:'stretch',
               marginLeft:130,
               marginBottom:30,
              },
  
        }
    );

