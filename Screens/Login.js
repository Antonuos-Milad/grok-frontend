import React, { Component } from 'react';
import {
    TextInput,
    StyleSheet,
    SafeAreaView,
    Alert,
    TouchableOpacity,
    Text,
    KeyboardAvoidingView,
    ToastAndroid,
    View,
    Image,

} from 'react-native';
import { Dimensions} from 'react-native'
import Colors from '../colors';
import { underline, bold } from 'ansi-colors';
import * as Font from 'expo-font';
import { AppLoading } from 'expo';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
// import Icon from 'react-native-vector-icons/FontAwesome';
// import { Ionicons } from '@expo/vector-icons';
//import SafeAreaView from 'react-native-safe-area-view';


export default class Login extends Component {
    state =
        {
            loginPassword: '', 
            user: '', 
            userObj: {},
            users: [], 
            userExist: false, 
            
            assetsLoaded: false,
        }

    async componentDidMount() {
        await Font.loadAsync({
            'Poppins': require('../assets/Fontt/Poppins-Bold.ttf'),
            'Poppins': require('../assets/Fontt/Poppins-Light.ttf'),
            'Poppins': require('../assets/Fontt/Poppins-SemiBold.ttf')
        });
    
        this.setState({ assetsLoaded: true });
    }

    //handle login 
    LoginHandler = () => {
      
        var url = 'http://192.168.137.91//api/Login';
        fetch(url) // get the list of users
            .then(response => response.json())
            .then(users => {
                this.setState({ users });
                
            }
            )
            .then(() => {
                //check if the user exists in the list of users
                this.state.users.forEach((item) => {
                    if (this.state.user.localeCompare(item.Username) == 0
                        && this.state.loginPassword.localeCompare(item.pass) == 0) {
                        this.setState({ userExist: true });
                        this.setState({ userObj: item });
                        return;
                    }
                });
            })
            .then(() => {
                // handle massege if the user exists or not
                if (this.state.userExist == true) {
                    ToastAndroid.showWithGravity(
                        'تم تسجيل الدخول بنجاح',
                        ToastAndroid.SHORT,
                        ToastAndroid.BOTTOM,
                    );

                    this.props.navigation.navigate('Sing', { user: this.state.userObj });
                }
                else {
                    Alert.alert(
                        ' ', // alert title
                        'اسم المستخدم أو كلمة المرور غير صحيحة، من فضلك حاول مرة أخرى', // alert message
                        [
                            {
                                text: 'حسنا'//, onPress: () => console.log('Ask me later pressed') 
                            },
                        ],
                        { cancelable: true },
                    );
                }
            })
            .catch(error => console.error('Error occured :: ', error));
    }


    //Navigation options for the stack navigator
    static navigationOptions =
        {
           headerVisible: false,
          header: null,
        };


       //is to render content within the safe area boundaries of a device
 //to solve the problem of keyboard
    render() {
        // const {assetsLoaded} = this.state;
        // if( assetsLoaded ) {
        return (
            <SafeAreaView style={styles.container}> 
        <Image source={require('../assets/images/Loginimg.png')} 
               style={{width: 100,
                      height: 150,
                      marginTop:60,
                      marginLeft:15,
                      backgroundColor: Colors.headerColor.color,
                      }} />
        <Text  fontfamily="Poppins-Bold" style={{
              backgroundColor: Colors.headerColor.color, fontWeight: 'bold',
             // fontfamily:'Poppins',
                alignSelf: 'flex-start',
                 textAlign: 'left',
               //  justifyContent:'center',
                 fontSize:30,
                 marginTop:2,
                 marginLeft:15,
                 color:'#ffffff',
            }}>Welcome, </Text>

            <Text  fontfamily="Poppins-Bold" style={{
              backgroundColor: Colors.headerColor.color,
             // fontfamily:'Poppins',
                alignSelf: 'flex-start',
                 textAlign: 'left',
               //  justifyContent:'center',
                 fontSize:25,
                 marginLeft:15,
                 marginBottom:10,
                 color:'#ffffff',  }}>Sign in to Continue .. </Text>
          

            <KeyboardAvoidingView behavior='position'>   
                    <TextInput
                        placeholder='User Name'
                        placeholderTextColor='#BFBFBF'
                        style={styles.inputs}
                        value={this.state.user}
                        onChangeText={(text) => {
                            this.setState({ user: text })
                        }
                        }
                     />

                    <TextInput
                        placeholder='Password'
                        placeholderTextColor='#BFBFBF'
                        style={styles.inputs}
                        secureTextEntry={true}
                        onChangeText={(text) => {
                            this.setState({ loginPassword: text })
                        }
                        }
                    />

                    <Text  style={{fontWeight:'bold',color: '#1D59CF', textAlign:'right',marginRight:27,fontSize:14}}
                    onPress={() =>  this.props.navigation.navigate('ForgetPassword')}>
                    forget password?
                    </Text>
              </KeyboardAvoidingView>

                <SafeAreaView style={styles.TouchableOpacityContainer}>
                    <TouchableOpacity  
                     onPress={this.LoginHandler} 
                     style={styles.confirmStyle}
                      activeOpacity={0.5}>
                     <Text style={styles.confirmTextStyle} > Get Started </Text>
                     <View style={styles.confirmSeparatorLine} />
                     <Image
                          source={require('../assets/images/forwardarrow/forward2.png')}
                          style={styles.confirmImageIconStyle} />
                  {/* <Text style={styles.TouchableOpacity}> get Started </Text>  */}
                    </TouchableOpacity>

                    <Text style={{color: '#1D59CF',textAlign: 'center', padding:8,fontSize:15}}>
                    Don't have an account ?   <Text style={{fontWeight: 'bold', color: '#1D59CF', textAlign: 'center',flexDirection:'row'}}
                    onPress={() =>  this.props.navigation.navigate('signup')}>
                    Sing up here
                    </Text>
                   </Text>
             
                <View style={styles.divider}>
                <View style={styles.hrLine} />
                <Text style={styles.dividerText}>OR</Text>
                <View style={styles.hrLine} />
                </View>

                <TouchableOpacity 
                  onPress={() => { this.props.navigation.navigate('faceboollogin');}}
                  style={styles.FacebookStyle}
                  activeOpacity={0.5}>
                    <Image
                     source={require('../assets/images/fbimages/f1.png')}
                     style={styles.ImageIconStyle} />
                    <View style={styles.SeparatorLine} />
                    <Text style={styles.TextStyle} > Login with Facebook </Text>
                </TouchableOpacity>

       {/* <TouchableOpacity 
                // onPress={() => { this.props.navigation.navigate('faceboollogin');}}
                  onPress={this.LoginHandler} >
                  style={styles.confirmStyle}
                  activeOpacity={0.5}>
                        <Image
                          source={require('../assets/images/forwardarrow/forward2.png')}
                          //Image Style
                          style={styles.ImageIconStyle} />
                      <View style={styles.SeparatorLine} />
                     <Text style={styles.confirmTextStyle} > get Started </Text>
                </TouchableOpacity>  */}

                </SafeAreaView>
            </SafeAreaView>
        );
    }
}

const { width } = Dimensions.get('window')
const styles = StyleSheet.create(
        {
            container: {
                flex:1,
                justifyContent: 'center',
                fontSize:16,
                backgroundColor: '#fd6f3f',
                color: Colors.inputColor.color,
            },
            inputs: {
                width: '90%',
                padding: 3,
                margin: 20,
                marginLeft: 15,
                marginBottom: 15,
                borderBottomWidth: 2,
                borderBottomWidth: StyleSheet.hairlineWidth,
                borderRadius: 8,
                borderBottomColor: '#ffffff',
                color: '#ffffff',
                textAlign: 'left',
               fontSize:15,
               fontWeight:'bold',
                justifyContent:'flex-start',
                
            },
            // TouchableOpacity: {
            //     flexDirection: 'row',
            //     justifyContent: 'space-around',
            //     alignItems: 'center',
            //     alignContent: 'center',
            //     textAlign: 'left',
            //     borderColor: '#1D59CF',
            //     borderWidth: 1,
            //     borderRadius: 10,
            //     fontSize: 18,
            //     fontFamily:'Poppins',
            //     width: 320,
            //     height:50,
            //     padding: 10,
            //     marginLeft: 20,
            //     marginTop: 30,
            //     backgroundColor: Colors.buttonsColor.color,
            //     color: Colors.inputColor.color,
            // },
            // TouchableOpacityfb: {
            //     flexDirection: 'row',
            //     justifyContent: 'space-around',
            //     alignItems: 'center',
            //     alignContent: 'center',
            //     textAlign: 'center',
            //     borderColor: '#1D59CF',
            //     borderWidth: 1,
            //     borderRadius: 10,
            //     fontSize: 18,
            //     fontFamily:'Poppins',
            //     width: 320,
            //     height:50,
            //     padding: 10,
            //     marginLeft: 20,
            //     marginTop: 30,
            //     backgroundColor:'white',
            //     color:'blue',

            // },

            divider: {
                flexDirection: 'row',
                alignItems: 'center',
                alignSelf: 'center',
                marginTop:25,
              },
              hrLine: {
                width: width / 2.6,
                backgroundColor: '#ffffff',
                height:1,
              },
              dividerText: {
                color: '#ffffff',
                textAlign: 'center',
                fontWeight:'bold',
                fontSize:15,
                alignSelf: 'center',
                width: width / 10 ,
              },

              FacebookStyle: {
                flexDirection: 'row',
                alignItems: 'center',
                alignContent: 'center',
                justifyContent: 'space-around',
                backgroundColor: '#ecf0f1',
                borderColor: 'white',
                borderWidth: 1,
                borderRadius: 10,
                fontFamily:'Poppins',
                width: 320,
                height:50,
                padding: 10,
                marginLeft: 20,
                marginTop: 30,
               
                
              },
              confirmStyle: {
                flexDirection: 'row',
                alignItems: 'center',
                alignContent: 'center',
                justifyContent: 'space-around',
                textAlign: 'left',
               borderColor: '#1D59CF',
                backgroundColor: '#1D59CF',
                borderWidth: 1,
                borderRadius: 10, 
                fontSize: 18,
                fontFamily:'Poppins',
                width: 320,
                height:50,
                padding: 10,
                marginLeft: 20,
                marginTop: 30,
                backgroundColor: Colors.buttonsColor.color,
              },


              ImageIconStyle: {
                padding: 10,
                margin: 5,
                height: 25,
                width: 25,
                resizeMode: 'stretch',
               
              },
            
              TextStyle: {
                color: '#1D59CF',
                marginBottom: 2,
                marginRight: 20,
                textAlign: 'center',
                fontSize: 18,
                fontFamily:'Poppins',
              },

              confirmTextStyle: {
                color: '#ffffff',
                marginBottom:2,
                marginRight: 130,
                textAlign: 'left',
                fontSize: 18,
                fontFamily:'Poppins',
              },
            
              SeparatorLine: {
                backgroundColor: 'blue',
                width: 1,
                height: 40,
              },

              confirmSeparatorLine: {
                backgroundColor: 'white',
                width: 1,
                height: 40,
                marginLeft:1,
                marginRight:2,
              },
              confirmImageIconStyle: {
              //  padding: 5,
                margin: 5,
                height: 25,
                width: 25,
                resizeMode: 'stretch',
               
              },


        }
    );
