import React, { Component } from 'react';
import {
    TextInput,
    StyleSheet,
    SafeAreaView,
    Alert,
    TouchableOpacity,
    Text,
    KeyboardAvoidingView,
    ToastAndroid,
    View,
    Image,
    StatusBar,
    ScrollView,
    useState,
    Button,
    Slider,
    TouchableHighlight,
} from 'react-native';
import { Constants } from 'expo';
// import Icon from 'react-native-vector-icons/FontAwesome';
//import SafeAreaView from 'react-native-safe-area-view';
//import { HeaderBackButton } from 'react-navigation-stack';
import { Ionicons} from '@expo/vector-icons';
import { Dimensions} from 'react-native'
import Colors from '../colors';
import { underline, bold } from 'ansi-colors';
import * as Font from 'expo-font';
import { AppLoading } from 'expo';
//import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
//import DatePicker from 'react-native-datepicker';
// import DateTimePickerModal from "react-native-modal-datetime-picker";
// import DateTimePicker from '@react-native-community/datetimepicker';
import PicUpload from './picUpload';


export default class Sing extends Component {

  static navigationOptions =
  {
    headerVisible: false,
    header: null,
  };

  async componentDidMount() {
        await Font.loadAsync({
            'Poppins': require('../assets/Fontt/Poppins-Bold.ttf'),
            'Poppins': require('../assets/Fontt/Poppins-Light.ttf'),
            'Poppins': require('../assets/Fontt/Poppins-SemiBold.ttf')
        });
    
        this.setState({ assetsLoaded: true });
    }
  
    state = 
    {
         username: '', 
         password: '',
         confirmPassword:'',
         email: '',
        // phone_number: '',

         passwordValid: false,
         confirmPassValid: false,
         emailValid: false,
         phoneValid: false,

         userValidNo: 0,

        //  date: new Date('2020-06-12T14:42:42'),
        //  mode: 'date',
        //  show: false,

         Age: 30,
         minAge: 10,
         maxAge: 100,

         gender: 'male',
         selectedButton: null,
         active: null,
         
    }
  
  selectionOnPress(userType) {
      this.setState({ selectedButton: userType });
  }

  //function for get age value
    getVal(val){
        console.warn(val);
        }     

//to set date of birth
    setDate = (event, date) => {
        date = date || this.state.date;
     
        this.setState({
          show: Platform.OS === 'ios' ? true : false,
          date,
        });
      }
     
      show = mode => {
        this.setState({
          show: true,
          mode,
        });
      }
     
      showDatepicker = () => {
        this.show('date');
      }
     
      // showTimepicker = () => {
      //   this.show('time');
      // }

//for set state for textinput
    onChangeText = (key, val) => {
        this.setState({ [key]: val })
   }

//    handlePickAvatar = async () => {
//     UserPermissions.getCameraPermission();
//     let result = await ImagePicker.launchImageLibraryAsync({
//         mediaTypes: ImagePicker.MediaTypeOptions.Images,
//         allowsEditing: true,
//         aspect: [4, 3]
//     });

//     if (!result.cancelled) {
//         this.setState({ user: { ...this.state.user, avatar: result.uri } });
//     }
// };

//sign up fuction start
   signUp = () => 
   {    
    
        if(this.state.userValidNo == 4)
        {
             let user = JSON.stringify(
                  {
                       Username: this.state.username,
                       pass: this.state.password,
                       email: this.state.email,
                      //  phone: this.state.phone_number,
                       Age:this.state.Age,
                  });
            

             let url = 'http://192.168.137.91/api/signup';
             fetch(url,
                  {
                       method: 'POST',
                       body: user,
                       headers: ({
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                       })
                  })
                  .then(res => res.json())
                  .then(response => {
                       
                       ToastAndroid.showWithGravity(
                            'تمت إضافة حسابك بنجاح',
                            ToastAndroid.SHORT,
                            ToastAndroid.BOTTOM,
                       );
                       this.props.navigation.navigate('login');
                  })
                  .catch(error => console.error('Failure signing up ::', error));
        }
        
        else
        {
             Alert.alert(
                  ' ', // alert title
                  'البيانات التي أدخلتها غير صحيحة، يرجى التأكد منها واعادة المحاولة', // alert message
                  [
                       {
                            text: 'حسنا'
                       }
                  ],
                  { cancelable: true },
             );
        }
   
   }

// validate user inputs
validate(text, type)
{
     if (type == 'password') {
          passRegex = /^[a - zA - Z0-9]\w{8,}$/;
          if (passRegex.test(text)) {
               this.setState({ passwordValid: true, userValidNo: this.state.userValidNo+1 });
    
          }
          else {
               Alert.alert(
                    ' ', // alert title
                    'يجب أن لا تقل كلمة المرور عن ثمانية احرف باللغة الانجليزية، من فضلك حاول مرة أخرى', // alert message
                    [
                         {
                              text: 'حسنا'
                         }
                    ],
                    { cancelable: true },
               );
          }
     }

     else if (type == 'confirmPassword') {

          if (this.state.confirmPassword.localeCompare(this.state.password) == 0) {
               this.setState({ confirmPassValid: true, userValidNo: this.state.userValidNo + 1 });
              
             
          }
          else {
               Alert.alert(
                    ' ', // alert title
                    'تأكيد كلمة المرور لا تتطابق مع كلمة المرور، من فضلك حاول مرة أخرى', // alert message
                    [
                         {
                              text: 'حسنا'
                         }
                    ],
                    { cancelable: true },
               );
          }
     }


     else if (type == 'email') {
          emailRegex = /^[a-z0-9]{3,}@(gmail|yahoo|outlook)\.(com|net|org)$/ig;
          if (emailRegex.test(text)) {
               this.setState({ emailValid: true, userValidNo: this.state.userValidNo + 1  });
               
            
          }
          else {
               Alert.alert(
                    ' ', // alert title
                    'صيغة البريد الالكتروني خاطئة، من فضلك حاول مرة أخرى', // alert message
                    [
                         {
                              text: 'حسنا'
                         }
                    ],
                    { cancelable: true },
               );
          }
     }

    //  else //(type == 'phone_number') 
    //  {
    //       phoneRegex = /(01)[0-9]{9}/g;
    //       if (phoneRegex.test(text)) {
    //            this.setState({ phoneValid: true, userValidNo: this.state.userValidNo + 1 });
              
              
    //       }
    //       else 
    //       {
    //            Alert.alert(
    //                 ' ', // alert title
    //                 'يجب أن لا يقل ولا يزيد رقم الهاتف عن 11 رقم، من فضلك حاول مرة أخرى', // alert message
    //                 [
    //                      {
    //                           text: 'حسنا'
    //                      }
    //                 ],
    //                 { cancelable: true },
    //            );
    //       }


    //  }

}   

//starting te view 
    render() {
        const { show, date, mode } = this.state;
        return (
            <SafeAreaView style={styles.container}>
            
             <View  style={{marginLeft:300,marginTop:40,marginBottom:-60,}}>
                 <Text style={{fontWeight:'bold',color:'white'}}>Skip</Text>
              </View>

            <View>
              <Text  fontfamily="Poppins-Bold" 
                     style={{ backgroundColor: Colors.headerColor.color,
                           // fontfamily:'Poppins',
                             alignSelf: 'flex-start',
                             textAlign: 'left',
                           //  justifyContent:'center',
                             fontSize:32,
                             marginTop:0,
                             marginLeft:22,
                             marginTop:60,
                             color:'#ffffff'}}>Singup</Text></View>
             
                      
                      {/* //seprator line */}
                      <View style={{flex: 1, flexDirection: 'row',justifyContent: 'space-between',marginLeft:22,marginRight:23,marginTop:15}}>
                      <View style={{width: 95, height: 6, backgroundColor: '#1D59CF',borderRadius: 10,}} />
                      <View style={{width: 95, height: 6, backgroundColor: '#1D59CF',borderRadius: 10,}} />
                      <View style={{width: 95, height: 6, backgroundColor: '#1D59CF',borderRadius: 10,}} />
                    </View> 
                    
                  <View style={{ position: "absolute", top: 2, alignItems: "center", width: "100%", marginBottom:100 }}>
                  <PicUpload/></View>
{/* 
                   <View style={{ position: "absolute", top: 2, alignItems: "center", width: "100%", marginTop:140, }}> 
                    <TouchableOpacity style={styles.avatarPlaceholder} 
                                     onPress={this.handlePickAvatar}>
                        <Image source={{ uri: this.state.avatar }} style={styles.avatar} />
                        <Ionicons
                            name="ios-camera"
                            size={40}
                            color="#FFF"
                            style={{ marginTop: 6, marginLeft: 2 }}
                        ></Ionicons>
                    </TouchableOpacity>
                    </View> */}
                   

                    <View style={{marginTop:130,}}>
                    <ScrollView>
                    <KeyboardAvoidingView behavior='absolute' enabled >
                                   <TextInput
                                        style={styles.inputs}
                                        placeholder='username '
                                        autoCapitalize="none"
                                        placeholderTextColor='#BFBFBF'
                                        value = {this.state.username}
                                        onChangeText={val => this.onChangeText('username', val)}
                              
                                   />
                                      <TextInput
                                        style={styles.inputs}
                                        placeholder='email'
                                        autoCapitalize="none"
                                        placeholderTextColor='#BFBFBF'
                                        value={this.state.email}
                                        onChangeText={val => this.onChangeText('email', val)}
                                        onEndEditing={(e) => this.validate(e.nativeEvent.text, 'email')}
                                   />

                                   <TextInput
                                        style={styles.inputs}
                                        placeholder='password'
                                        secureTextEntry={true}
                                        autoCapitalize="none"
                                        placeholderTextColor='#BFBFBF'
                                        value={this.state.password}
                                        onChangeText={val => this.onChangeText('password', val)}
                                        onEndEditing={(e) => this.validate(e.nativeEvent.text, 'password')}
                                   />

                                   {/* <TextInput
                                        style={styles.inputs}
                                        placeholder='Confirm Password'
                                        secureTextEntry={true}
                                        autoCapitalize="none"
                                        placeholderTextColor='#BFBFBF'
                                        value={this.state.confirmPassword}
                                        onChangeText={val => this.onChangeText('confirmPassword', val)}
                                        onEndEditing={(e) => this.validate(e.nativeEvent.text, 'confirmPassword')}
                                   /> */}

                                   {/* <TextInput
                                        style={styles.inputs}
                                        placeholder='Phone Number'
                                        autoCapitalize="none"
                                        placeholderTextColor='#BFBFBF'
                                        keyboardType='numeric'
                                        value = {this.state.phone_number}
                                        onChangeText={val => this.onChangeText('phone_number', val)}
                                        onEndEditing={(e) => this.validate(e.nativeEvent.text, 'phone_number')}
                                   />  */}
                                    </KeyboardAvoidingView>  
                    </ScrollView> 
                    </View>
                    {/* //choice GenerType */}
                    {/* <View style={{flexDirection: 'row',  justifyContent: 'space-between'} }>
                              <TouchableOpacity style={styles.GenderbuttonStyle1} 
                                                activeOpacity={0.5} title="male"
                                                onPress={() => {this.setState({gender: 'male',  })}}>
                                              {/* //  selected={this.setState({touchedColor: '#fff',})} */}
                                              {/* <Text> Male </Text>
                              </TouchableOpacity>
                            
                              <TouchableOpacity style={styles.GenderbuttonStyle2} 
                                                activeOpacity={0.5} title="Female"
                                                onPress={() => { this.setState({gender: 'female',})}}
                                               Selected ={ this.state.pressed? backgroundColor: "green" }>
                                              <Text> female </Text>
                              </TouchableOpacity>
                              </View> */} 
                               
               {/* <View style={{flexDirection: 'row',  justifyContent: 'space-between'} }>
                    <Text style={{color:'white' ,marginLeft:15,}}> iam</Text>

                    <TouchableOpacity  active= {{backgroundColor:'#333', borderColor: '#333',  color: '#eee',}}
                       style={styles.GenderbuttonStyle1} 
                          onPress={() => this.selectionOnPress("Male") && this.setState({gender: 'male'})}>
                    <Text  style={{ backgroundColor:  this.state.selectedButton === "Male" ? "#ececec" : "grey", padding:20,paddingLeft:30,paddingRight:30 }} >
                    <Text style={{padding:20,margin:50,color:'#d0d0d0',fontWeight:'bold'}}>Male</Text> </Text> 
                   </TouchableOpacity>     

                    <TouchableOpacity  style={styles.GenderbuttonStyle2} 
                                   onPress={() => this.selectionOnPress("female") && this.setState({gender: 'female',})}>
                    <Text style={{ backgroundColor: this.state.selectedButton === "female" ? "white" : "grey" }} >
                    <Text >female</Text>   </Text>
                   </TouchableOpacity>
                </View> */}
                     <Text style={{color:'white' ,marginLeft:15,}}> iam</Text>
                <View style={{ flexDirection: 'row',  justifyContent: 'center', }}>
                  <View >
                      <TouchableOpacity 
                          onPress={() => {this.setState({ active: 0 })}}
                          style={ this.state.active === 0 ? styles.btnActive : styles.btn }>
                      <Text style={{color:'#fff',fontWeight:'bold',marginBottom:10}}>
                      <Image
                          source={require('../assets/images/maleFemaleIcons/maleOutline.png')}
                          //Image Style
                         style={styles.confirmImageIconStyle} />
                       male </Text>
                      </TouchableOpacity>
                  </View>

                  <View >
                      <TouchableOpacity  
                          onPress={() => {this.setState({ active: 1 })}}
                          style={ this.state.active === 1 ? styles.btnActive : styles.btn }>
                      <Text style={{color:'#fff',fontWeight:'bold',marginBottom:10}}>
                      <Image
                          source={require('../assets/images/maleFemaleIcons/FemaleOutline.png')}
                          //Image Style
                         style={styles.confirmImageIconStyle} /> female 
                       </Text>
                      </TouchableOpacity>
                   </View></View>

                        {/* age slider */}
                          <Text style={{color:'white',marginLeft:15,marginBottom:15,textAlign:'left' }}> my age is </Text> 
                        <View style={styles.ageSlider}><View >
                        
                           </View> 
                      <Slider
                           style={{ width: 345,height:20}}
                           step={1}
                           thumbTouchSize={{width: 200, height:100}}
                           minimumValue={this.state.minAge}
                           maximumValue={this.state.maxAge}
                           value={this.state.Age}
                           onValueChange={val => this.setState({ Age: val })}
                           thumbTintColor='#1D59CF' 
                           trackStyle= {{width: 100, height: 1000}}
                           maximumTrackTintColor='#d3d3d3' 
                           minimumTrackTintColor='#fff' />
       
                       <View style={styles.textCon}>
                          {/* <Text style={styles.MaxMinColor}>{this.state.minAge} yrs</Text> */}
                          <Text style={styles.MaxMinColor}>{this.state.minAge}</Text>
                          <Text style={styles.ValueColor}>
                                {/* {this.state.Age + 'yrs'} */}
                                {this.state.Age}
                          </Text>
                          {/* <Text style={styles.MaxMinColor}>{this.state.maxAge}yrs</Text> */}
                          <Text style={styles.MaxMinColor}>{this.state.maxAge}</Text>
                       </View>
                            </View>

                    {/* //singup Confirm Button */}
                        <TouchableOpacity  
                                onPress={this.handleSignUp}
                                style={styles.confirmStyle}
                                activeOpacity={0.5}>
                                  
                                <Text style={styles.confirmTextStyle} > Get Started </Text>
                                <View style={styles.confirmSeparatorLine} />
                                <Image
                                     source={require('../assets/images/forwardarrow/forward2.png')}
                                     //Image Style
                                     style={styles.confirmImageIconStyle} />
                             {/* <Text style={styles.TouchableOpacity}> get Started </Text>  */}
                        </TouchableOpacity>

                 {/* data&time buttons */}
                            {/* <View>
                                <View>
                                  <Button onPress={this.showDatepicker} title="Show date picker!" />
                                </View>
                                 <View>
                                  <Button onPress={this.showTimepicker} title="Show time picker!" />
                                </View> 
                                  { show && <DateTimePicker value={date} mode={mode} is24Hour={true} display="default"  onChange={this.setDate} /> }
                            </View>     */}
               </SafeAreaView>
       );
    }
}

const { width } = Dimensions.get('window')
const styles = StyleSheet.create(
        {
            container: {
                flex:1,
                justifyContent: 'center',
                fontSize:15,
                backgroundColor: '#fd6f3f',
                color: Colors.inputColor.color,

            },

            avatarPlaceholder: {
                width: 100,
                height: 100,
                backgroundColor: "#E1E2E6",
                borderRadius: 50,
                marginTop: 10,
                justifyContent: "center",
                alignItems: "center"
            },
            avatar: {
                position: "absolute",
                width: 100,
                height: 100,
                borderRadius: 50,
                alignItems: 'center',
                width: '100%',
                // marginLeft:50,
                // marginRight:50,
               
            },

            inputs: {
                width: '90%',
                padding: 3,
                margin: 10,
                marginLeft: 15,
                marginBottom: 15,
                borderBottomWidth: 1,
               // borderBottomWidth: StyleSheet.hairlineWidth,
                borderRadius: 8,
                borderBottomColor: '#ffffff',
                color: '#ffffff',
                textAlign: 'left',
                fontSize:15,
               // fontWeight:'bold',
                justifyContent:'flex-start',
                
            },

              confirmStyle: {
                flexDirection: 'row',
                alignItems: 'center',
                alignContent: 'center',
                justifyContent: 'space-around',
                textAlign: 'left',
                borderColor: '#1D59CF',
                backgroundColor: '#1D59CF',
                borderWidth: 1,
                borderRadius: 10, 
                fontSize: 18,
                fontFamily:'Poppins',
                width: 320,
                height:50,
                padding: 10,
                marginLeft: 20,
                marginTop: 15,
                marginBottom:20,
                backgroundColor: Colors.buttonsColor.color,
              },

              confirmTextStyle: {
                color: '#ffffff',
                marginBottom:2,
                marginRight: 130,
                textAlign: 'left',
                fontSize: 18,
                fontFamily:'Poppins',
              },

              confirmSeparatorLine: {
                backgroundColor: 'white',
                width: 1,
                height: 40,
                marginLeft:1,
                marginRight:2,
              },
              confirmImageIconStyle: {
              //  padding: 5,
                margin: 5,
                height: 25,
                width: 25,
                resizeMode: 'stretch',
               
              },


       //age slider start
       ageSlider: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fd6f3f',
       // backgroundColor: '#fff',
        padding:30,
        marginRight:8,
        marginBottom:20,
      },
       textCon: {
        width: 315,
        flexDirection: 'row',
        justifyContent: 'space-between',
        color: '#fff',
      },
      MaxMinColor: {
        color: '#fff',
        fontWeight:'bold',
      },
      ValueColor: {
        color: '#1D59CF',
        fontWeight:'bold',
        fontSize:35,
        textDecorationLine: 'underline',

      },
      //age slider end

      //gender button style

  btn: {
    flexDirection: 'column',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    backgroundColor: '#DDDDDD',
    borderColor: '#BFBFBF',
    borderRadius: 10,
    borderWidth: 1.5,
    padding: 10,
    width: 150,
    height:50,
    fontSize: 18,
    fontFamily:'Poppins',
    marginLeft: 4,
    marginTop: 15,
    marginBottom:30,
    marginRight:10,
    
  //  backgroundColor: Colors.buttonsColor.color,
  backgroundColor: 'transparent',
  },

  btnActive: {
    flexDirection: 'column',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    backgroundColor: '#fd8b65',
    
    borderColor: '#fff',
    borderRadius: 10,
    borderWidth: 1,
    padding: 10,
    width: 150,
    height:50,
    fontSize: 18,
    fontFamily:'Poppins',
    marginLeft: 4,
    marginTop: 15,
    marginBottom:30,
    marginRight:10,
  },
        }
    );

